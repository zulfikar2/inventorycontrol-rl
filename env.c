#include "env.h"

double* dataP;
int M;

double* initEnvData(char* fileDir) {
	FILE *file;
	
	double K = -1, c = -1, r = -1, h = -1, z = -1;
	
	if((file = fopen(fileDir, "r")) == NULL) {
		printf("file1.txt does not exist");
		exit(1);
	}
	char buffer[BUFFER];
	int count = 0;
	while(fgets(buffer, BUFFER, file) != NULL) {
		//M at count 0
		if (count == 0) {
			M = atoi(buffer)+1;
			printf("M = %d\n", M-1);
			dataP = calloc((M+7), sizeof(double));
		}
		//K at count M+2
		else if (count == M+1) {
			K = atof(buffer);
			printf("K = %f\n", K);
		}
		//c at count M+3
		else if (count == M+2) {
			c = atof(buffer);
			printf("c = %f\n", c);
		}
		//r at count M+4
		else if (count == M+3) {
			r = atof(buffer);
			printf("r = %f\n", r);
		}
		//h at count M+5
		else if (count == M+4) {
			h = atof(buffer);
			printf("h = %f\n", h);
		}
		//z at count M+6
		else if (count == M+5) {
			z = atof(buffer);
			printf("z = %f\n", z);
		}
		dataP[count] = atof(buffer);
		count++;
	}
	fclose(file);
	return dataP;
}

int offset(int x, int y, int z) { 
	//return ( z * xSize * ySize ) + ( y * xSize ) + x ;
	return ( z * M * M ) + ( y * M ) + x;
} 

double max (double v1, double v2) {
	if(v1 > v2)
		return v1;
	else
		return v2;
}

//returns random demand Dk
int getDemand() {
	int demand = 0;
	double probability = (double)rand() / (double)RAND_MAX;
	double culmProbability = 0;
	//iterate until culmulative probability >= random probability
	for(int i = 1; i <= M; i++) {
		culmProbability += dataP[i];
		if(culmProbability >= probability) {
			demand = i;
			break;
		}
	}
	return demand;
}

void printPolicy(double *order, int iterations) {
	if(iterations != -1)
		printf("\nNumber of iterations : %d\n", iterations);
	if(iterations != -1)
		printf("\nFinal Policy : \n");
	else
		printf("\nInitial Policy : \n");
	
	for(int s = 0; s < M; s++) {
		printf("Policy for state %d : %.4f\n", s, order[s]);
	}
}

double absolute(double v1, double v2) {
	if(v1> v2) {
		return v1-v2;
	}
	else {
		return v2-v1;
	}
}