#include "pia.h"
#include "math.h"

#define DISCOUNT 0.95
#define PRECISION 0.0001

int policyStable;
double *fileData;

int M, iterations;
double K, c, r, h, z;
double *P;
double *R;
double *V;
//policy
double *order;
double *C;

int policyEval();
int	policyImprovement();
void initializeP();
double stateUpdateP(int s);
int argMaxA(int s);
double stateAddP(int xk, int ak, int c);

int policy_iteration(double* data) {
	
	fileData = data;
	initializeP(data);
	iterations = 0;
	
	do {
		policyEval();
		policyStable = policyImprovement();
		iterations++;
		
	}while(policyStable == 0);
	
	printPolicy(order, iterations);
}

void initializeP() {
	//data[0] = M
	//data[M+2 - M+6] = K, c, r, h, z
	printf("\nPolicy Iteration:\n");
	M = fileData[0]+1;
	K = fileData[M+1]; //max inventory size
	c = fileData[M+2]; //wholesale cost
	r = fileData[M+3]; //retail unit price
	h = fileData[M+4]; //holding unit cost
	z = fileData[M+5]; //penalty of unsatisfied demand
	printf("vals M %d K %f c %f r %f h %f z %f\n", M-1, K, c, r, h, z);
	
	iterations = -1;
	
	P = calloc(M*M*M, sizeof(double));
	R = calloc(M*M*M, sizeof(double));
	V = calloc(M, sizeof(double));
	order = calloc(M, sizeof(double));
	C = calloc(M, sizeof(double));
	
	//how much to order at each state (INITIAL POLICY)
	for(int s = 0; s <= M; s++) {
		order[s] = M/2 - 1;
		if(order[s] <= 0)
			order[s] = 1;
	}
	
	//print inital policy
	printPolicy(order, iterations);
	
	//probabilities P[s][a][s']
	for(int s = 0; s < M; s++) {
		for(int a = 0; a < M; a++) {
			for(int sP = 0; sP < M; sP++) {
				int d = s+a-sP;
				if(d>0 & d<M)
					P[offset(s, a, sP)] = fileData[d];
				else
					P[offset(s, a, sP)] = 0;
			}
		}
	}
	//total ordering cost
	for(int a = 0; a < M; a++) {
		C[a] = c * a + K;
	}
	
	//rewards R[s][a][s']
	for(int s = 0; s < M; s++) {
		for(int a = 0; a < M; a++) {
			for(int sP = 0; sP < M; sP++) {
				if(sP > (s+a)) {
					R[offset(s, a, sP)] = 0;
				}
				else {
					R[offset(s, a, sP)] = r * (s + a) - C[a] - z;
				}
			}
		}
	}
	
	//states V[s]
	for(int s = 0; s < M; s++) {
		V[s] = 0;
	}
}

int policyEval() {
	int delta = 0;
	//Policy Evaluation
	do {
		delta = 0;
		//for each s in S
		for (int s = 0; s < M; s++) {
			//v = V(s)
			double v = V[s];
			//V[s] = P(Pi)ss'* [R(Pi)ss' + DISCOUNT*V[s']]
			V[s] = stateUpdateP(s);
			delta = max(delta, absolute(v, V[s]));
		}
		//printf("delta : %d\n", delta);
	}while (delta >= PRECISION);
	return 1;
}

//V[s] =P(Pi)ss'* [R(Pi)ss' + DISCOUNT*V[s']]
double stateUpdateP(int xk) {
	double v;
	int ak = order[xk];
	
	v = stateAddP(xk, ak, 0);
	
	return v;
}


int	policyImprovement() {
	
	double b;
	//Policy Improvement
	int policyStable = 1;
	//for each s in S
	for(int s = 0; s <= M; s++) {
		//b = Pi[s]
		b = order[s];
		//Pi[s] = argMaxA * Pass' * [Rass' + DISCOUNT*V[s']]
		order[s] = argMaxA(s);
		//if b != Pi[s], then policyStable = false
		if(b != order[s]) {
			policyStable = 0;
			break;
		}
	}
	//if policyStable, then stop; else go back to policyEval
	return policyStable;
}

double stateAddP(int xk, int ak, int c) {
	if(xk+ak-c <= 0)
		return 0;
	else
		
	return P[offset(xk,ak,xk+ak-c)] * (R[offset(xk,ak,xk+ak-c)] + (DISCOUNT*V[xk+ak-c])) + stateAddP(xk,ak,c+1);
}
int argMaxA(int s) {
	int maxR = -1;
	int maxA = -1;
	double val[M];
	//try every possibility
	for(int x = 0;x < M-s; x++) {
		//do all actions and record rewards
		val[x] = stateAddP(s,x,0);
	}
	
	//return best action
	for(int x = 0; x < M-s; x++) {
		if(val[x] > maxA) {
			maxR = val[x];
			maxA = x;
		}
	}
	if(maxA < 0)
		return 0;
	return maxA;
}