# Inventory Control with uncertain demand

A simplified model of inventory control with uncertain demand
for a certain stocked commodity. The commodity whose inventory is being controlled is assumed to be indivisible
(i.e., it occurs in integer quantities) and deterioration of stock is excluded (i.e., for simplicity, pilferage, spoilage, and
deterioration are assumed not to occur).

![](Explanation1.png)
![](Explanation2.png)