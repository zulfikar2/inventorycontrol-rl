#include "via.h"
#include "math.h"

#define DISCOUNT 0.95
#define PRECISION 0.0001

int policyStable;
double *fileData;

int M, iterations;
double K, c, r, h, z;
double *P;
double *R;
double *V;
//policy
double *order;
double *C;

int maxA(int s);
double stateAdd(int s, int a, int s1);
double stateUpdate(int xk);
void initalize();

void initialize() {
	//data[0] = M
	//data[M+2 - M+6] = K, c, r, h, z
	printf("\nValue Iteration:\n");
	M = fileData[0]+1;
	K = fileData[M+1]; //max inventory size
	c = fileData[M+2]; //wholesale cost
	r = fileData[M+3]; //retail unit price
	h = fileData[M+4]; //holding unit cost
	z = fileData[M+5]; //penalty of unsatisfied demand
	printf("vals M %d K %f c %f r %f h %f z %f\n", M-1, K, c, r, h, z);
	
	iterations = -1;
	
	P = calloc(M*M*M, sizeof(double));
	R = calloc(M*M*M, sizeof(double));
	V = calloc(M, sizeof(double));
	order = calloc(M, sizeof(double));
	C = calloc(M, sizeof(double));
	
	//how much to order at each state (INITIAL POLICY)
	for(int s = 0; s < M; s++) {
		order[s] = M/2 - 1;
		if(order[s] <= 0)
			order[s] = 1;
	}
	
	//print inital policy
	printPolicy(order, iterations);

	//probabilities P[s][a][s']
	for(int s = 0; s < M; s++) {
		for(int a = 0; a < M; a++) {
			for(int sP = 0; sP < M; sP++) {
				int d = s+a-sP;
				if(d>0 && d<M) {
					P[offset(s, a, sP)] = fileData[d];
				}
				else {
					P[offset(s, a, sP)] = 0;
				}
			}
		}
	}
	
	//total ordering cost
	for(int a = 0; a < M; a++) {
		C[a] = c * a + K;
	}
	
	//rewards R[s][a][s']
	for(int s = 0; s < M; s++) {
		for(int a = 0; a < M; a++) {
			for(int sP = 0; sP < M; sP++) {
				int d = s+a-sP;
				if(d > (s+a)) {
					R[offset(s, a, sP)] = 0;
				}
				else {
					R[offset(s, a, sP)] = r * (s + a) - C[a] - z;
				}
			}
		}
	}
	
	//states V[s]
	for(int s = 0; s < M; s++) {
		V[s] = 0;
	}
	
}

int value_iteration(double *data) {
	fileData = data;
	initialize();
	
	iterations = 0;
	int delta = 0;
	double v = 0;
	do {
		delta = 0;
		//for each s in S
		for(int s = 0; s < M; s++) {
			//v <- V[s]
			v = V[s];
			//V[s] <- maxa * Pass' * (Rass' + DISCOUNT*V[s'])
			V[s] = stateUpdate(s);
			//delta <- max(delta, |v-V[s]|)
			delta = max(delta, absolute(v, V[s]));
		}
		iterations++;
	}while(delta >= PRECISION);
	
	//output deterministic policy
	for(int s = 0; s <= M; s++) {
		order[s] = maxA(s);
	}
	printPolicy(order, iterations);
}

//V[s] = maxA * Pass' * (Rass' + DISCOUNT*V[s']
double stateUpdate(int xk) {
	double v;
	int ak = order[xk];
	
	v = maxA(xk) * stateAdd(xk, ak, 0);
	
	return v;
}

//s' can be :
double stateAdd(int xk, int ak, int c) {
	if(xk+ak-c <= 0)
		return 0;
	else
		
	return P[offset(xk,ak,xk+ak-c)] * (R[offset(xk,ak,xk+ak-c)] + (DISCOUNT*V[xk+ak-c])) + stateAdd(xk,ak,c+1);
}

int maxA(int s) {
	int maxR = -1;
	int maxA = -1;
	double val[M];
	//try every possibility
	for(int x = 0;x < M-s; x++) {
		//do all actions and record rewards
		val[x] = stateAdd(s,x,0);
	}
	
	//return best action
	for(int x = 0; x < M-s; x++) {
		if(val[x] > maxA) {
			maxR = val[x];
			maxA = x;
		}
	}
	if(maxA < 0)
		return 0;
	return maxA;
}