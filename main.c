#include "env.h"
#include "pia.h"
#include "via.h"

int main() {
	
	srand((unsigned)time(NULL));
	double *data = initEnvData("file1.txt");
	//calls policy iteration algorithm with data
	value_iteration(data);
	policy_iteration(data);
	
	return 0;
}