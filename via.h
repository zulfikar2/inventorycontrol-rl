#ifndef VIA
#define VIA
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "env.h"

int value_iteration(double* data);

#endif