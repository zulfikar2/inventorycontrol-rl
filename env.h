#ifndef ENV
#define ENV
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define BUFFER 255

double* initEnvData(char* fileDir);
int offset(int x, int y, int z);
double max(double v1, double v2);
int getDemand();
void printPolicy(double *order, int iterations);
double absolute(double v1, double v2);

#endif